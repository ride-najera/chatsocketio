var config=require('./config/config');
var server=require('./app');
var mongoose=require('mongoose');

mongoose.Promise=global.Promise;

mongoose.connect(config.getConn,function(err,res){
if(err) throw err;

console.log("Conexion a mongo establecida!!!");

server.listen(config.port,function(){
	console.log("corriendo en el puerto: "+config.port+"...");
});

});

