var express=require('express');
var app=express();
var realTime=require('./RealTime/events');
var bodyParser=require('body-parser');
var http=require('http');


//
var server=http.Server(app);


var RoutesUser=require('./routes/routesUser');
var RoutesChat=require('./routes/routesChat');
var RouteIndex=require('./routes/routeIndex');
var path=require('path');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());



app.set("view engine","jade");


// var public=express.static(path.join(__dirname,'public'));
app.use(express.static(path.join(__dirname,'public')));
// app.use(public);
app.use('/app',express.static(path.join(__dirname,'public')));




app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});



app.use('/',RouteIndex);
app.use('/api',RoutesUser);
app.use('/app',RoutesChat);



realTime(server);


module.exports=server;