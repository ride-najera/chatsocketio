var express=require('express');
var router=express.Router();

var UserController=require('../controllers/UserController');


router.get('/registro',UserController.getRegistro);

router.get('/login',UserController.getLogin);

router.post('/registro',UserController.setRegistro);

module.exports=router;
