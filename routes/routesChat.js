var express=require('express');
var router=express.Router();
var ChatController=require('../controllers/ChatController');

router.get('/chat',ChatController.getChat);

module.exports=router;