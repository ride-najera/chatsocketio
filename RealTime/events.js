module.exports=function(server){

var Message=require('../Models/Message');

var io=require('socket.io')(server);

var nicknames=[];
var msj_private;

io.on('connection',function(socket){
var flag=0;

//--------------------------registar user------------------------------------------

socket.on('registrar',function(namesid){

nicknames.forEach(function(nickname,item){
   if( nickname.user===namesid.user ){

       nicknames.splice(item,1);
       nicknames.push(namesid);

       flag=1;

   }else{
      
      flag=0;
        
        }    

});

if( flag==0 ){
console.log(namesid.user+" se ah conectado!")  
   nicknames.push(namesid);
  }

io.sockets.emit('users',nicknames);

});

//--------------------------registar user------------------------------------------</>

//--------------------------mensaje privado------------------------------------------
socket.on('privado',function(privado){

if(privado.idu=="empty"){

   
Message.find({},function(err,rows){
if(err) throw err;

msj_private=rows;
   socket.emit('only-me',msj_private);  
});



}else{

  var message=new Message();

message.idu=privado.idu; 
message.nombre=privado.nombre;
message.idpropio=privado.idpropio;
message.destinatario=privado.destinatario;
message.hora=privado.hora;
message.msj=privado.msj;

message.save(function(err,messageStored){
if(err) throw err;

Message.find({},function(err1,rows){
if(err1) throw err1;

msj_private=rows;

   socket.to(privado.idu).emit('only-you',msj_private);

   socket.emit('only-me',msj_private);

});


});



}
// io.sockets.in
// socket.to//well
});
//--------------------------mensaje privado------------------------------------------

//--------------------------user deconectado------------------------------------------
socket.on("disconnect",function(){
var name_discconnected;

nicknames.forEach(function(nickname,item){

   if(nickname.iduser==socket.id){
       name_discconnected=nickname.user;
       nicknames.splice(item,1);
      }

   });

   io.sockets.emit('users',nicknames);
   //socket.broadcast.emit("desconectado",{message:`El Ususario ${name_discconnected} se ah desconectado`});
   console.log(name_discconnected+ " se ah desconectado");

});

//--------------------------user deconectado------------------------------------------

});


                                 }